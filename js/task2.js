"use strict";

/* 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути,
    наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

const button = document.createElement("button");
button.id = "btn-input-create";

const contentSpace = document.querySelector("#content");
contentSpace.append(button);
contentSpace.style.display = "flex";
contentSpace.style.flexDirection = "column";
contentSpace.style.alignItems = "center";
// display: flex;
//     align-content: center;
//     flex-direction: column;
//     /* justify-content: center; */
//     /* flex-wrap: wrap; */
//     align-items: center;

button.innerText = "Hello there";

button.addEventListener("click", () => {
    const inputItem = document.createElement("input");
    inputItem.type = "submit";
    inputItem.placeholder = "";
    inputItem.name = "someName";
    contentSpace.append(inputItem);
    console.log(button, inputItem);
});



