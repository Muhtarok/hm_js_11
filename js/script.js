"use strict";

/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з 
  текстом "New Paragraph" і додайте його до розділу <section id="content">
 */


console.log("QUESTIONS");

console.log("1. Подія - це сигнал від браузера, що щось трапилося, всі вузли надають такі сигнали");
console.log("2. Події клікання кнопками, пересування мишки (наведення). Приклади: click, context, mouseover, mousedown");
console.log("3. Це подія, яка, зазвичай, привязана до кліку правою кнопкою миші, ця подія супроводжується викликом налаштувань, меню, додаткових параметрів, даних про якийсь обʼєкт");

console.log("Practical TASKS");
console.log("TASK 1");

const clickButton = document.querySelector("#btn-click");
const contentSpace1 = document.querySelector("#content");

clickButton.addEventListener("click", () => {
   let someParagraph = document.createElement("p");
   someParagraph.id = "some-paragraph";
   someParagraph.innerText = "New Paragraph";
   contentSpace1.append(someParagraph);
});






